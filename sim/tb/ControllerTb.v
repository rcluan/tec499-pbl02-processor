module ControllerTb ();

parameter Halfcycle = 5;

localparam Cycle = 2*Halfcycle;

reg Clock, Reset;

initial Clock = 0;
always #(Halfcycle) Clock = ~Clock;

reg Start, Tx_Busy;
reg WriteEnable;
reg [4:0] Address;
reg [31:0] Data_In;

wire Tx_Start;
wire [7:0] Tx_Data;

Controller ctrl (
    .Clock(Clock),
    .Start(Start),
    .Tx_Busy(Tx_Busy),
    .Address(Address),
    .WriteEnable(WriteEnable),
    .Data_In(Data_In),

    .Tx_Start(Tx_Start),
    .Tx_Data(Tx_Data)
);

initial begin
    Start = 0;
    Tx_Busy = 0;
    WriteEnable = 0;
    Address = 0;
    Data_In = 0;

    #(Cycle);
    WriteEnable = 1;

    Address = 0; Data_In = 1; #(Cycle);
    Address = 1; Data_In = 2; #(Cycle);
    Address = 2; Data_In = 3; #(Cycle);
    Address = 3; Data_In = 4; #(Cycle);
    Address = 4; Data_In = 5; #(Cycle);
    Address = 5; Data_In = 6; #(Cycle);
    Address = 6; Data_In = 7; #(Cycle);
    Address = 7; Data_In = 8; #(Cycle);
    
    WriteEnable = 0;
    #(Cycle);

    Start = 1; 
    #(Cycle);

    repeat(10) begin
        //Tx_Busy = 1; #(4*Cycle);
        Tx_Busy = 0; #(Cycle);
        //Tx_Busy = 1; #(4*Cycle);
        Tx_Busy = 0; #(Cycle);
        Tx_Busy = 1; #(Cycle);
        //Tx_Busy = 1; #(4*Cycle);
        Tx_Busy = 0; #(Cycle);
        //Tx_Busy = 1; #(4*Cycle);
        Tx_Busy = 0; #(Cycle);
        #(Cycle);
        #(Cycle);
        #(Cycle);
        #(3*Cycle); 
    end
    $finish();
end

endmodule