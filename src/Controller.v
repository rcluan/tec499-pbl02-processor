module Controller (
    input Clock,
    input Start,
    input Tx_Busy,
    input [4:0] Address,
    input WriteEnable,
    input [31:0] Data_In,

    output reg Tx_Start,
    output reg [7:0] Tx_Data

);

localparam BUF_SIZE = 32;
localparam addr_width = $clog2(BUF_SIZE);

reg [31:0] buffer [0:(BUF_SIZE-1)];

reg [(addr_width-1):0] addr = 0;
reg [2:0] counter = 0;

localparam IDLE = 3'b000;
localparam COMM = 3'b001;
localparam WAIT = 3'b010;
localparam INC  = 3'b011;

reg [2:0] state = IDLE;

integer i;
initial begin
    for (i = 0; i < BUF_SIZE; i=i+1)
        buffer[i] <= 0;
end

always @(posedge Clock) begin
    buffer[Address] <= WriteEnable ? Data_In : buffer[Address];
end

always @(posedge Clock) begin

    case (state)
        IDLE: 
            if (Start) begin
                state <= COMM;
                counter <= 0;
                addr <= 0;
            end
        
        COMM:
            if (~Tx_Busy) begin
                if (counter < 4) begin
                    case (counter)
                        3'b000: Tx_Data  <=  buffer[addr][7:0];
                        3'b001: Tx_Data  <=  buffer[addr][15:8];
                        3'b010: Tx_Data  <=  buffer[addr][23:16];
                        3'b011: Tx_Data  <=  buffer[addr][31:24];
                    endcase
                    Tx_Start <= 1;
                    state <= WAIT;
                    counter <= counter + 1;
                end
                else begin
                    counter <= 0;
                    state <= INC;
                end
            end
            else
                state <= WAIT;
        WAIT: begin
            if (~Tx_Busy) state <= COMM;
            Tx_Start <= 0;
            end
        INC:
            if (addr == BUF_SIZE-1) state <= IDLE;
            else begin
                addr <= addr + 1;
                state <= COMM;
            end
    endcase
end

endmodule