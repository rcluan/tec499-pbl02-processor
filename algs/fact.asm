# Calcula o fatorial de n
# Resultado final vai estar em v0 ao fim da execucao
# v0 = fact(n)
     .text
    main:
        li $a0, 4   # n = 4, coloque o valor de n desejado nesta linha
        jal fact    # fact(n)
        j exit      # pula pro fim do arquivo
    fact:
        addi $sp, $sp, -8   # libera 2 espacos na pilha
        sw $ra, 4($sp)      # salva endereco de retorno 
        sw $a0, 0($sp)      # salva n

        li $v0, 1           # v0 = 1
        slt $t0, $zero, $a0 # t0 = (0 < n ) ? 1:0
        beq $t0, $zero, fact_return # if (t0 == 0) goto fact_return
        addi $a0, $a0, -1   # n--
        jal fact            # fact(n-1)
        lw $a0, 0($sp)      # carrega n da pilha
        mul $v0, $v0, $a0   # v0 = v0 * n
    fact_return:
        lw $ra, 4($sp)      # carrega endereco de retorno da pilha
        addi $sp, $sp, 8    # libera o espaco alocado da pilha
        jr $ra              # goto endereco de retorno
    exit:
