Processador 
==============

Linguagem de descrição de Hardware
----------
* Verilog

Instruções Implementadas
-------------------
```
add rd, rs, rt	
addi rt, rs, immediate	
sub rd, rs, rt	
lui rt, immediate	
sll rd, rt, sa	
ori rt, rs, immediate	
mul rd, rs, rt	
div rs, rt	
mfhi rd	
slt rd, rs, rt	
beq rs, rt, offset	
bne rs, rt, offset	
j address	
jal address	
jr rs	
lw rt, offset(base)	
sw rt, offset(base)	
```
-------------------
Modo de uso
-------------------
* Abra o arquivo Processor.qpf no Quartus II
* Compile o projeto recém aberto
* Abra o arquivo Processor.mpf (presente na pasta simulation/modelsim) no ModelSim
* Substitua os caminhos de FILE_IN e $readmemb nos arquivos AlgBubbleTb, AlgFactTb, AlgFibonacciTb, AlgPotenciaTb, AlgPrimesTb e AlgRaizTb da pasta tb pelo caminho absoluto onde o repositório foi clonado
* Remova a task checkRegisterFile nos arquivos AlgBubbleTb, AlgFactTb, AlgFibonacciTb, AlgPotenciaTb, AlgPrimesTb e AlgRaizTb (essa task só deve ser utilizada para testes funcionais)
* Compile todos os arquivos na aba Project
* Vá na aba Simulate > Start Simulation
* Em Start Simulation:
* Design -> escolha o teste desejado (e.g. AlgBubbleTb, AlgFactTb, etc.)
* Libraries -> adicione as bibliotecas altera_ver e cycloneive_ver
* SDF -> adicione o arquivo Processor_v.sdo e indique o "Apply to Region" como /cpu
* SDF -> marque as caixas "Reduce SDF warnings" e "Reduce SDF errors to warnings"
* Clique em OK
* Na aba sim, recém aberta, clique em cpu e adicione-o aos waves
* Na aba Transcript do Modelsim digite run -all