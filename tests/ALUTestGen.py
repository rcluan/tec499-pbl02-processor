#!/usr/bin/python

import random
import os

def bin(x, width):
    if x < 0: x = (~x) + 1
    return ''.join([(x & (1 << i)) and '1' or '0' for i in range(width-1, -1, -1)])


def sext(a):
    if not a & (1<<15):
        return a
    else:
        return 0xffff0000 | (abs(a)&0xffff)

def bwnot(a):
    return reduce(lambda a,b: a|b, [([1,0][a>>x & 1]) << x for x in range(0,32)])

def flipsign(a):
    if a < 0:
        return bwnot(abs(a)) + 1
    elif a & (1<<31):
        return -(bwnot(a) + 1)
    else:
        return a

def comp(a,b):
    a = flipsign(a)
    b = flipsign(b)
    return a < b

def sub(a,b):
    res = a-b 
    if res < 0:
        return bwnot(abs(res)) + 1
    else:
        return res


# tuple contains (opcode/funct bits, function, limit for a, limit for b)
opcodes = \
{ 
    "RTYPE":   ("000000", lambda a,b: 0, lambda a: a, lambda b: b, "000"),
    "R2TYPE":  ("011100", lambda a,b: 0, lambda a: a, lambda b: b, "000"),
    "LW":      ("100011", lambda a,b: a+b, lambda a: a, lambda b: sext(b&0xffff), "010"),
    "SW":      ("101011", lambda a,b: a+b, lambda a: a, lambda b: sext(b&0xffff), "010"),
    "ADDI":    ("001001", lambda a,b: a+b, lambda a: a, lambda b: sext(b&0xffff), "010"),
    "J":       ("000010", lambda a,b: a+b, lambda a: a, lambda b: sext(b&0xffff), "010"),
    "JAL":     ("000011", lambda a,b: a+b, lambda a: a, lambda b: sext(b&0xffff), "010"),
    "ORI":     ("001101", lambda a,b: a|b, lambda a: a, lambda b: abs(b)&0xffff, "011"),
    "LUI":     ("001111", lambda a,b: b<<16, lambda a: a, lambda b: abs(b)&0xffff, "001"),
    "BEQ":     ("000100", lambda a,b: sub(a,b), lambda a: a, lambda b: abs(b)&0xffff, "100"),
    "BNE":     ("000101", lambda a,b: sub(a,b), lambda a: a, lambda b: abs(b)&0xffff, "100")
}

functs = \
{
    "SLL":     ("000000", lambda a,b: b<<a, lambda a: a&0x1f, lambda b: b),
    "ADD":     ("100001", lambda a,b: a+b, lambda a: a, lambda b: b),
    "JR":      ("001000", lambda a,b: a+b, lambda a: a, lambda b: b),
    "SUB":     ("100010", lambda a,b: sub(a,b), lambda a: a, lambda b: b),
    "SLT":     ("101010", lambda a,b: (lambda:0, lambda:1)[comp(a,b)](), lambda a: a, lambda b: b),
}               

functs2 = \
{
    "MUL":     ("000010", lambda a,b: a*b, lambda a: a, lambda b: b)
}
                
random.seed(os.urandom(32))
file = open('alutestvector.input', 'w')

def gen_vector(op, f, a, b, opcode, funct, ctrl):
    A = a(random.randint(0, 0xffffffff))
    B = b(random.randint(0, 0xffffffff))
    REFout = f(A,B)
    # Uncomment this if you want to see decimal outputs
    #print 'Op: {0}, A: {1}, B: {2}, Out: {3}, Opcode: {4}, Funct: {5}'.format(op, str(A), str(B), str(REFout),opcode, funct)
    #return ''
    return ''.join([opcode, funct, bin(A, 32), bin(B, 32), bin(REFout, 32), ctrl])

loops = 50

for i in xrange(loops):
    for opcode, tup in opcodes.iteritems():
        oc, f, a, b, ctrl = tup
        if opcode == "RTYPE":
            for funct, tup in functs.iteritems():
                fct, f, a, b = tup
                file.write(gen_vector(funct, f, a, b, oc, fct, ctrl) + '\n')
                # print gen_vector(funct, f, a, b, oc, fct)
        elif opcode == "R2TYPE":
            for funct, tup in functs2.iteritems():
                fct, f, a, b = tup
                file.write(gen_vector(funct, f, a, b, oc, fct, ctrl) + '\n')
        else:
            fct = bin(random.randint(0, 0x3f), 6)
            file.write(gen_vector(opcode, f, a, b, oc, fct, ctrl) + '\n')
            # print gen_vector(opcode, f, a, b, oc, fct)
        

