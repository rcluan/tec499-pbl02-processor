`timescale 1ns / 1ps
`include "Opcodes_Functs_Parameters.vh"
`include "ALU_Parameters.vh"
module ALUTestVector ();

    parameter Halfcycle = 5;

    localparam Cycle = 2*Halfcycle;

    reg Clock;

    initial Clock = 0;
    always #(Halfcycle) Clock = ~Clock;

    reg [5:0] funct;
    reg [5:0] opcode;
    reg [2:0] AluCtrl;
    reg [4:0] shamt;
    reg [31:0] A,B;
    reg reset;

    wire [31:0] AluOut;
    reg [31:0] RefOut;
    wire [3:0] AluOp;

    wire ZERO,OVERFLOW,UNDERFLOW,NEGATIVE,CARRY;

    ALUControl AluControl (
        .Funct(funct),
        .ALUCtrl(AluCtrl),
        .ALUOp(AluOp)
    );

    ALU alu (
        .Clock(Clock),
        .Reset(reset),
        .A(A),
        .B(B),
        .Shamt(shamt),
        .ALUOp(AluOp),
        .ALUOut(AluOut),
        .ZERO(ZERO),
        .UNDERFLOW(UNDERFLOW),
        .OVERFLOW(OVERFLOW),
        .NEGATIVE(NEGATIVE),
        .CARRY(CARRY)
    );


    task checkOutput;
        input [31:0] A,B;
        input [4:0] shamt;
        input [31:0] AluOut, RefOut;
        input [5:0] funct;
        input [2:0] AluCtrl;
        begin
            $display ("Funct: %b  AluCtrl: %b A: %d B: %d  shamt: %d",funct,AluCtrl,A,B,shamt);
            $display ("AluOut: %d  RefOut: %d",AluOut,RefOut);
            if (AluOut == RefOut) begin
                $display("Pass!\n");
            end
            else begin
                $display("Failed.\n");
                $finish();
            end
        end
    endtask

    localparam testCases = 750;
    reg [110:0] testvector [0:(testCases-1)];

    integer i;
    initial begin
        $readmemb("sim/tests/alutestvector.input",testvector);
        reset = 0;
        #(Cycle);
        for (i = 0; i < testCases; i=i+1) begin
            opcode = testvector[i][110:105];
            funct = testvector[i][104:99];
            A = testvector[i][98:67];
            B = testvector[i][66:35];
            RefOut = testvector[i][34:3];
            AluCtrl = testvector[i][2:0];
            shamt = A[4:0];
            #(Cycle);
            checkOutput(A,B,shamt,AluOut,RefOut,funct,AluCtrl);
        end


        $finish();
    end
endmodule
